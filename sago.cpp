#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <assert.h>
#include <iostream>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>


using namespace std;

#include "board.h"
#include "feature.h"
#include "common.h"

int *mcnum_b;
#include "playout.h"
#include "ladder.h"
uint64_t shash[4][121];
#include "pattern.h"



void getgtpz(int z,char *sxy)
{
  if ( z==0 ) return;
  int y = z / 11;    
  int x = z - y*11; 

  if(x<9){
    sxy[0] = ('A'+x-1);
  }
  else{
    sxy[0] = 'A'+x;
  }

  if(y>9){
    sxy[1] = ('0'+y/10);
    sxy[2] = ('0'+(y%10));
    sxy[3]='\0';
  }
  else {
    sxy[1]=('0'+y);
    sxy[2]='\0';
  }
}


void send_gtp(const char *fmt, ...)
{
 va_list ap;

 va_start(ap, fmt);
 vfprintf( stdout, fmt, ap );  
 va_end(ap);
}

int main()
{


	//FILE *filepr = fopen("uint64list.dat","r");
	FILE *filepr = fopen("uint64list.dat","r");


	for (int i = 0; i < 4; ++i)
	for (int j = 0; j < 121; ++j)
		shash[i][j] = random64();
		//fscanf(filepr, "%llu", &shash[i][j]);

	/*
	for (int i = 0; i < 4; ++i)
	for (int j = 0; j < 121; ++j)
		printf( "%llu\n", shash[i][j]);
	*/

	srand((unsigned)time(NULL));
	Board *b = new Board;
	init_board(b,19,6.5);
	init_board_data(b);
	mcnum_b=(int *)malloc(sizeof(int)*b->bmax);

	char coordc[]={'A','B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','S','T'};





	char str[256];
	//
	int gtpz,gtpx,gtpy;

	setbuf(stdout, NULL);
	setbuf(stderr, NULL);  
	for (;;) {
	if ( fgets(str, 256, stdin)==NULL ) break; 



	if ( strstr(str,"list_commands")   ) {
	 send_gtp("= boardsize\nclear_board\ngenmove\ngogui_analyze_commands\nkomi\nlist_commands\nname\nplay\nprotocol_version\nversion\nshowboard \n\n"); 

	}else if      ( strstr(str,"boardsize 19")   ) {
	 send_gtp("= \n\n");  
	}

	else if ( strstr(str,"clear_board") ) {
		delete(b);
		b = new Board;
		init_board(b,19,6.5);
		init_board_data(b);
		send_gtp("= \n\n"); 
	}
	//     fprintf( stderr, "gtpz=%d\n",gtpz);

	else if ( strstr(str,"showboard") ) {
		int x,y;
    	char str[3]={'.','x','o'};
    	//printf("%d %d %d %d\n", b->dir4[0], b->dir4[1], b->dir4[2], b->dir4[3] );
    	//cout <<  "   ";
    	//for (x=0; x<b->bsize; x++) {cout <<(x+1) << " ";}
    	//cout << endl;
    	send_gtp("= \n"); 
    	for (y=b->bsize-1; y>=0; y--) {
        //cout<<setprecision(2)<<(y+1) << ": ";
        for (x=0; x<b->bsize; x++) {
            send_gtp( "%d ",b->data[get_z(b,x,y)]);//str[b->data[get_z(b,x,y)]
        }
        send_gtp( "\n");
    }
	 send_gtp(" \n\n"); 
	}

	else if ( strstr(str,"name")        ) {
	 send_gtp("= sagoyc\n\n");
	}else if ( strstr(str,"version")     ) {
	send_gtp("= 0.0.1\n\n");
	}else if ( strstr(str,"genmove w")   ) {
		int *kouho=(int *)malloc(sizeof(int)*b->bmax);
	    int kouho_num = 0;
	    int rx,ry;
	    for (ry=0;ry<b->bsize;ry++) for (rx=0;rx<b->bsize;rx++) {
	       
	      int rz = get_z(b,rx,ry);
	      if ( b->data[rz] != 0 ) continue;
	      
	       
	      kouho[kouho_num] = rz;
	      kouho_num++;

	    }

	    int rz,rr = 0;
	    for (;;) {
	      if ( kouho_num == 0 ) {
	        rz = 0;
	      } else {
	        
	        rr = rand() % kouho_num;   // 乱数で1手選ぶ
	        rz = kouho[rr];


	      }
	      
	      
	      int err = move(b,rz,2);
	   
	      //printf("%d \n",  3);
	      if ( err >= 0 ) {
	      	//fprintf( stderr, "= %c%d\n\n",coordc[get_x(b,z)],get_y(b,z)+1);
	      	          break;}

	      kouho[rr] = kouho[kouho_num-1];  // エラーなので削除
	      kouho_num--;
	  }
	  free(kouho);

	     //gtpz = z;
	     //fprintf( stderr, "gtpz=%d\n",z);
	     if ( rz > 0) {
	      // move(b,gtpz,2);
	       send_gtp("= %c%d\n\n",coordc[get_x(b,rz)],get_y(b,rz)+1);
	     }else if(rz==0)send_gtp("= PASS\n\n");

	}else if ( strstr(str,"genmove b")   ) {
		int *kouho=(int *)malloc(sizeof(int)*b->bmax);
	    int kouho_num = 0;
	    int rx,ry;
	    for (ry=0;ry<b->bsize;ry++) for (rx=0;rx<b->bsize;rx++) {
	       
	      int rz = get_z(b,rx,ry);
	      if ( b->data[rz] != 0 ) continue;
	      
	       
	      kouho[kouho_num] = rz;
	      kouho_num++;

	    }

	    int rz,rr = 0;
	    for (;;) {
	      if ( kouho_num == 0 ) {
	        rz = 0;
	      } else {
	        
	        rr = rand() % kouho_num;   // 乱数で1手選ぶ
	        rz = kouho[rr];


	      }
	      
	      
	      int err = move(b,rz,1);
	   
	      //printf("%d \n",  3);
	      if ( err >= 0 ) {
	      	//fprintf( stderr, "= %c%d\n\n",coordc[get_x(b,z)],get_y(b,z)+1);
	      	          break;}

	      kouho[rr] = kouho[kouho_num-1];  // エラーなので削除
	      kouho_num--;
	  }
	  free(kouho);

	     //gtpz = z;
	     //fprintf( stderr, "gtpz=%d\n",z);
	     if ( rz > 0) {
	      // move(b,gtpz,2);
	       send_gtp("= %c%d\n\n",coordc[get_x(b,rz)],get_y(b,rz)+1);
	     }else if(rz==0)send_gtp("= PASS\n\n");

	}else if ( strstr(str,"gogui_analyze_commands")        ) {
	 send_gtp("= sboard/show_groupid/show_groupid\n");
	 send_gtp("sboard/show_dame/show_dame\n");
	 send_gtp("sboard/show_oppsnum/show_oppsnum\n ");
	 send_gtp("sboard/show_gamma/show_gamma\n");
	 send_gtp("cboard/show_cgamma/show_cgamma\n");
	 send_gtp("sboard/show_mconum/show_mconum\n");
	 send_gtp("sboard/show_checkladder/show_checkladder\n");
	 send_gtp("sboard/show_hashb/show_hashb\n");
	 send_gtp("sboard/show_hashw/show_hashw\n");
	 send_gtp("\n\n");
	}else if ( strstr(str,"show_hashb")        ) {
		int x,y;
	 	send_gtp("=  ");
	        

	    for (y=b->bsize-1; y>=0; y--) {
	        for (x=0; x<b->bsize; x++)
		        send_gtp( " %llu",b->positionhashb[4][get_z(b,x,y)]);
	        send_gtp( "\n");
		}
		send_gtp("\n\n");
	}else if ( strstr(str,"show_hashw")        ) {
		int x,y;
	 	send_gtp("=  ");


	    for (y=b->bsize-1; y>=0; y--) {
	    	for (x=0; x<b->bsize; x++) 
		        send_gtp( " %llu",b->positionhashw[4][get_z(b,x,y)]);
	        send_gtp( "\n");
		}
		send_gtp("\n\n");


	}else if ( strstr(str,"show_checkladder")        ) {
		int x,y;
	 	send_gtp("=  ");
	 	Board ladderb = *b;

	    for (y=b->bsize-1; y>=0; y--) {
	        for (x=0; x<b->bsize; x++) {
	        	int gid = b->z_to_groupid[get_z(b,x,y)];
	        	if (gid>=0)
	        	{
		        	if (b->group[gid].lib_z.size()==2)
		        	{
		        		//Board ladderb = *b;
		        		send_gtp( " %d",check_ladder_datasafe(b,gid,5));
		        	}else
		            	send_gtp( " \"\"");
	            }else
		            	send_gtp( " \"\"");
	        }
	        send_gtp( "\n");
		}
		send_gtp("\n\n");
	}else if ( strstr(str,"show_mconum")        ) {
		int x,y;
	 	send_gtp("=  ");

	 	
	 	for (int mcn = 0; mcn < b->bmax; ++mcn)
	    	mcnum_b[mcn] = 0;

	    Board mcb;
	    for (int i = 0; i < 64; ++i)
	    {
	    	mcb = *b;
	    	playout_mco(&mcb,1);
	    }


	    for (y=b->bsize-1; y>=0; y--) {
	        for (x=0; x<b->bsize; x++) {
	            send_gtp( " %d",mcnum_b[get_z(b,x,y)]);
	        }
	        send_gtp( "\n");
		}
		send_gtp("\n\n");
	}else if ( strstr(str,"show_cgamma")        ) {
		int x,y;
	 	send_gtp("=  ");
		for (int mcn = 0; mcn < b->bmax; ++mcn)
    		mcnum_b[mcn] = 0;

	    Board mcb;
	    for (int i = 0; i < 64; ++i)
	    {
	    	mcb = *b;
	    	playout_mco(&mcb,1);
	    }

	 for (y=b->bsize-1; y>=0; y--) {
	        //cout<<setprecision(2)<<(y+1) << ": ";
	        for (x=0; x<b->bsize; x++) {
	        	//int gid = b->z_to_groupid[get_z(b,x,y)];

	        	int tz = get_z(b,x,y);
	        	float val= calcu_feature_value(b,tz,1);
	        	val = val/(val+0.6);
	        	val *= 766;

	        	int color = int(val);

	        	int rr ;
	        	if (color>766)
	        	{
	        		rr = 255;
	        	}
	        	else if (color>510)
	        	{
	        		rr = color - 510;
	        	}else 
	        		rr = 0;

	        	int gg ;
	        	if (color>512)
	        	{
	        		gg = 255;
	        	}else if (color > 256)
	        	{
	        		gg = color -256;
	        	}else
	        		gg = 0;



	        		int bb;
	        	if (color>255)
	        	{
	        		bb = 255;
	        	}else{
	        		bb = color;
	        	}


	            send_gtp(" #");
	            send_gtp("%02x",rr);
		    	send_gtp("%02x",gg);
		    	send_gtp("%02x",bb);
	        }
	        send_gtp( "\n");
		}
		fprintf( stderr, "lastmove=%d %d\n",get_x(b,b->last_move1),get_y(b,b->last_move1));
		send_gtp("\n\n");
	}else if ( strstr(str,"show_gamma")        ) {
		int x,y;
	 send_gtp("=  ");
	 		for (int mcn = 0; mcn < b->bmax; ++mcn)
    		mcnum_b[mcn] = 0;

	    Board mcb;
	    for (int i = 0; i < 64; ++i)
	    {
	    	mcb = *b;
	    	playout_mco(&mcb,1);
	    }
	 for (y=b->bsize-1; y>=0; y--) {
	        //cout<<setprecision(2)<<(y+1) << ": ";
	        for (x=0; x<b->bsize; x++) {
	        	//int gid = b->z_to_groupid[get_z(b,x,y)];
	        	int tz = get_z(b,x,y);
	        	float val = calcu_feature_value(b,tz,1);
	        	//val = val/(val+1);
	        	if (val!=0)
	        	{
	        		send_gtp( " %d",val);
	        	}else send_gtp( " \"\"");
	        	
	        }
	        send_gtp( "\n");
		}
		//fprintf( stderr, "lastmove=%d %d\n",get_x(b,b->last_move1),get_y(b,b->last_move1));
		send_gtp("\n\n");
	}else if ( strstr(str,"show_groupid")        ) {
		int x,y;
	 send_gtp("=  ");
	 for (y=b->bsize-1; y>=0; y--) {
	        //cout<<setprecision(2)<<(y+1) << ": ";
	        for (x=0; x<b->bsize; x++) {
	        	int gid = b->z_to_groupid[get_z(b,x,y)];
	        	//send_gtp(" #");
		    	//send_gtp("%02x",i*10);
		    	//send_gtp("%02x",i*10);
		    	//send_gtp("%02x",abs(255 - i*2));
	            send_gtp( " %d",gid);
	        }
	        send_gtp( "\n");
		}
		send_gtp("\n\n");
	}else if ( strstr(str,"show_oppsnum")        ) {
		int x,y;
	 send_gtp("=  ");
	 for (y=b->bsize-1; y>=0; y--) {
	        //cout<<setprecision(2)<<(y+1) << ": ";
	        for (x=0; x<b->bsize; x++) {
	        	int gid = b->z_to_groupid[get_z(b,x,y)];
	        	int oppsnum;
	        	if (gid!=-1)
	        		oppsnum = b->group[gid].neighbor_opp.size();
	        	else
	        		oppsnum = 0;
	        	//send_gtp(" #");
		    	//send_gtp("%02x",i*10);
		    	//send_gtp("%02x",i*10);
		    	//send_gtp("%02x",abs(255 - i*2));
	            send_gtp( " %d",oppsnum);
	        }
	        send_gtp( "\n");
		}
		send_gtp("\n\n");
	}else if ( strstr(str,"show_dame")        ) {
		int x,y;
	 	send_gtp("=  ");
	 	for (y=b->bsize-1; y>=0; y--) {
	        //cout<<setprecision(2)<<(y+1) << ": ";
	        for (x=0; x<b->bsize; x++) {
	        	int gid = b->z_to_groupid[get_z(b,x,y)];
	        	int dame;
	        	if (gid!=-1)
	        		dame = b->group[gid].lib_z.size();
	        	else
	        		dame = 0;
	        	//send_gtp(" #");
		    	//send_gtp("%02x",i*10);
		    	//send_gtp("%02x",i*10);
		    	//send_gtp("%02x",abs(255 - i*2));
	            send_gtp( " %d",dame);
	        }
	        send_gtp( "\n");
		}
		send_gtp("\n\n");
	}

	else if ( strstr(str,"play")        ) {
		int mcolor,mvx,mvy;
		//printf("%c %c %c\n",str[5],str[7],str[8] );
		if (str[5] == 'B')mcolor = 1;
		else mcolor = 2;

		if (str[7] >= 'J')mvx = str[7]-'A'-1;
		else mvx = str[7]-'A';


		int temp = str[9]-'0';

		if (temp<10&&temp>=0)mvy = (str[8]-'0')*10+(str[9]-'1');
		else mvy = str[8] - '1';


		//fprintf( stderr, "%d %d %d\n",mcolor,mvx,mvy);

		int err =move(b,get_z(b,mvx,mvy),mcolor);
		fprintf( stderr, "err = %d\n",err);

		send_gtp("= \n\n");
		//print_board(b);

	}
	else {
	 send_gtp("= \n\n");  // ���������������������������������OK���������
	}
	}
	return 0;
	//*/

	/*
	char str[256];
	int gtpz,gtpx,gtpy;
	initboard();
	gtpz = select_best_uct(1);
	move(gtpz,1);
	print_board();
	gtpz = select_best_uct(2);
	move(gtpz,2);
	print_board();
	*/

}


