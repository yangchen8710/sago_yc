#include <vector>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <stdio.h>
#include <stdint.h>
using namespace std;


#include "common.h"
#include "boost/lexical_cast.hpp"

template <typename T> std::string to_string(const T& arg) {
  try {
    return boost::lexical_cast<std::string>(arg);
  }
  catch(boost::bad_lexical_cast& e) {
    return "";
  }
}
//

static inline uint32_t random32()
{
  static uint64_t state = 0;
  state = state * 6364136223846793005UL + 1442695040888963407UL;
  return state >> 32;
}

uint64_t random64()
{
  uint64_t x0 = random32();
  uint64_t x1 = random32();
  return x0 | (x1 << 32);
}


void unique_int_vec(std::vector<int> *v)
{

    sort(v->begin(),v->end()); 
    v->erase( unique( v->begin(), v->end() ), v->end() );
}

void del_item_int_vec(std::vector<int> *v,int i)
{


    remove(v->begin(),v->end(),i);
    v->pop_back();
}

int flip_color(int col) {
  return 3 - col; // 石の色を反転させる
}