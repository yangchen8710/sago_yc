#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>

using namespace std;


void getgtpz(int z,char *sxy)
{
  if ( z==0 ) return;
  int y = z / 11;    
  int x = z - y*11; 

  if(x<9){
    sxy[0] = ('A'+x-1);
  }
  else{
    sxy[0] = 'A'+x;
  }

  if(y>9){
    sxy[1] = ('0'+y/10);
    sxy[2] = ('0'+(y%10));
    sxy[3]='\0';
  }
  else {
    sxy[1]=('0'+y);
    sxy[2]='\0';
  }
}


void send_gtp(const char *fmt, ...)
{
 va_list ap;

 va_start(ap, fmt);
 vfprintf( stdout, fmt, ap );  
 va_end(ap);
}

int main()
{

 char str[256];
 //
 int gtpz,gtpx,gtpy;

 setbuf(stdout, NULL);
 setbuf(stderr, NULL);  
 for (;;) {
   if ( fgets(str, 256, stdin)==NULL ) break; 



  if ( strstr(str,"list_commands")   ) {
     send_gtp("= boardsize\nclear_board\ngenmove\ngg-undo\ngogui_analyze_commands\nkomi\nlist_commands\nname\nplay\nprotocol_version\nversion \n\n"); 

   }

   else if      ( strstr(str,"boardsize 19")   ) {
     send_gtp("= \n\n");  
   }

   else if ( strstr(str,"clear_board") ) {
     send_gtp("= \n\n"); 
   }

   else if ( strstr(str,"name")        ) {
     send_gtp("= my_analyzer\n\n");
   }

   else if ( strstr(str,"version")     ) {
    send_gtp("= 0.0.1\n\n");
   }

   else if ( strstr(str,"genmove w")   ) {
    send_gtp("= PASS\n\n");
   }

   else if ( strstr(str,"genmove b")   ) {
   }

   // ��������������������� "play W D17" ������������������
   else if ( strstr(str,"gogui_analyze_commands")        ) {
     send_gtp("= cboard/my_analyzer/my_analyzer\n\n");
   }


   else if ( strstr(str,"my_analyzer")        ) {
     send_gtp("=  ");
     //int counter = 1;
     for (int i = 0; i < 361; ++i)
     { 
      //counter*=2;

        send_gtp(" #");
        send_gtp("%02x",i);
        send_gtp("%02x",i);
        send_gtp("%02x",abs(255 - i));


      
      if ((i+1)%19==0)
      {
        send_gtp("\n");
      }
       
     }
     send_gtp("\n\n");
   }


   else if ( strstr(str,"play")        ) {
     send_gtp("= \n\n");

   }

   else {
     send_gtp("= \n\n");  // ���������������������������������OK���������
   }

 }
 return 0;
 //*/

/*
 char str[256];
 int gtpz,gtpx,gtpy;
 initboard();
 gtpz = select_best_uct(1);
 move(gtpz,1);
 print_board();
 gtpz = select_best_uct(2);
 move(gtpz,2);
 print_board();
*/

}


