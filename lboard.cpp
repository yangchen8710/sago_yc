#include <vector>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdint.h>
typedef struct LGroup
{
	int id;
	int color;
	//int member_num;
	std::vector<int> member_z;
	//int lib_num;
	std::vector<int> lib_z;
	std::vector<int> neighbor_opp;
	int status;
}LGROUP;

class LBoard{ 
public:
	int bsize;
	int bwidth;
	int bmax;
	std::vector<GROUP> group;
	std::vector<int> data;
	std::vector<int> z_to_groupid;
	float komi;
	int dir4[5];
	int ko_z;
	int last_move1;
	int lm1c;
	//std::vector<int> lm1_neighbor_oppz;
	int last_move2;
	uint64_t positionhashw[9][500];
	uint64_t positionhashb[9][500];
};



void init_board(Board *b, int bsize,float komi);

void init_board_data(Board *b);

void print_board(Board *b);

int get_x(Board *b,int z);

int get_y(Board *b,int z);

int get_z(Board *b,int x,int y);

int get_rgroup_id(Board *b,int gid);

void capture_group(Board *b,int gid,std::vector<int> *changed_z);

void merge_groups(Board *b,int gid1,int gid2,int tz);

int move(Board *b,int tz,int color);


bool isLegal (LBoard *lb, int color, int z) {
	if(z == 0) return true;
	if(lb->data[z] != 0||z == lb->ko_z) return false;

	
  // check for suicide
  if (nbr_cnt[v].empty_cnt () > 0) return true;
  bool not_suicide = false;

  vertex_for_each_4_nbr (v, nbr_v, chain_at (nbr_v).lib_cnt -= 1);

  vertex_for_each_4_nbr (v, nbr_v, {
    bool atari = chain_at (nbr_v).lib_cnt == 0;
    not_suicide |=
      color_at [nbr_v].IsPlayer () &
      (atari != (color_at [nbr_v].ToPlayer () == player));
  });

  vertex_for_each_4_nbr (v, nbr_v, chain_at(nbr_v).lib_cnt += 1);

  return not_suicide;
}