#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;

#include "common.h"
#include "board.h"
#include "ladder.h"

int check_ladder_datasafe(Board *b,int gid,int depth)
{

	//return b->group[gid].lib_z.size();
	if (b->group[gid].lib_z.size()>2)
	{
		return 1;
	}
	if (b->group[gid].lib_z.size()<2)
	{
		return -1;
	}
	//if(b->group[b->group[gid].neighbor_opp[0]].lib_z.size()==1)
	//return b->group[gid].neighbor_opp[0]*10+b->group[gid].neighbor_opp[1];
	for (int i = 0; i < b->group[gid].neighbor_opp.size(); ++i)
	{
	  	if(b->group[b->group[gid].neighbor_opp[i]].lib_z.size()==1)
	  		return 1;
	}

	if (depth == 0)
	{
		return 0;
	}

	

	int color = b->group[gid].color ;
	int un_col = flip_color(color);
	int libz0 = b->group[gid].lib_z[0];
	int libz1 = b->group[gid].lib_z[1];
	int escapez_flag0 = 0;//by moving this libz ,1 escape,2
	int escapez_flag1 = 0;//by moving this libz ,1 escape,2

	std::vector<int> oppgroups0;//id
	std::vector<int> frigroups0;//id
	std::vector<int> spaces0;//4方向の空白のz
	std::vector<int> walls0;//4方向の盤外のz



	for (int i=0;i<4;i++){
		int z = libz0+b->dir4[i];
		int c = b->data[z]; // 石の色
		if ( c == 0 && z!=libz1) 
			spaces0.push_back(z);
		else if ( c == color ) 
			frigroups0.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == un_col )
			oppgroups0.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == 3 ) 
			walls0.push_back(z);
	}




	
	unique_int_vec(&frigroups0);
	unique_int_vec(&oppgroups0);
	del_item_int_vec(&frigroups0,gid);	






	//if (frigroups0.size()>0)
	for (int m = 0; m < frigroups0.size(); ++m)
	{
		for (int i = 0; i < b->group[frigroups0[m]].lib_z.size(); ++i)
		{
			if (b->group[frigroups0[m]].lib_z[i]!=libz0)
			{
				spaces0.push_back(b->group[frigroups0[m]].lib_z[i]);
			}
			
		}

	}
	unique_int_vec(&spaces0);

	if (spaces0.size()>2)
	{
		escapez_flag0 = 1;
	}else if (spaces0.size()==2)
	{
		escapez_flag0 = 0;
	}else escapez_flag0 = -1;

	//if (oppgroups0.size()>0)
	for (int m = 0; m < oppgroups0.size(); ++m)
	{	
		if (b->group[oppgroups0[m]].lib_z.size()==2&&spaces0.size()==2)
		{
			escapez_flag0 = 1;
		}
		if (b->group[oppgroups0[m]].lib_z.size()==1&&b->group[oppgroups0[0]].lib_z[0]!=libz0)
		{
			escapez_flag0 = 1;
		}
	}

	if (escapez_flag0 == -1)
		return -1;





	std::vector<int> oppgroups1;//id
	std::vector<int> frigroups1;//id
	std::vector<int> spaces1;//4方向の空白のz
	std::vector<int> walls1;//4方向の盤外のz

	for (int i=0;i<4;i++){
		int z = libz1+b->dir4[i];
		int c = b->data[z]; // 石の色
		if ( c == 0 && z!=libz0 ) 
			spaces1.push_back(z);
		else if ( c == color ) 
			frigroups1.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == un_col ) 
			oppgroups1.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == 3 ) 
			walls1.push_back(z);
	}

	unique_int_vec(&frigroups1);
	unique_int_vec(&oppgroups1);
	del_item_int_vec(&frigroups1,gid);



	//if (frigroups1.size()>0)
	for (int m = 0; m < frigroups1.size(); ++m)
	{
		for (int i = 0; i < b->group[frigroups1[m]].lib_z.size(); ++i)
		{
			if (b->group[frigroups1[m]].lib_z[i]!=libz1)
				spaces1.push_back(b->group[frigroups1[m]].lib_z[i]);
		}
	}
	unique_int_vec(&spaces1);

	if (spaces1.size()>2)
	{
		escapez_flag1 = 1;
	}else if (spaces1.size()==2)
	{
		escapez_flag1 = 0;
	}else escapez_flag1 = -1;

	//if (oppgroups1.size()>0)
	for (int m = 0; m < oppgroups1.size(); ++m)
	{	
		if (b->group[oppgroups1[m]].lib_z.size()==2&&spaces1.size()==2)
		{
			escapez_flag1 = 1;
		}
		if (b->group[oppgroups1[m]].lib_z.size()==1&&b->group[oppgroups1[m]].lib_z[0]!=libz1)
		{
			escapez_flag1 = 1;
		}
	}



	if (escapez_flag0==-1||escapez_flag1==-1)
	{
		return -1;
	}	
	if (escapez_flag0==1&&escapez_flag1==1)
	{
		return 1;
	}


	if (escapez_flag0==0&&escapez_flag1==1)
	{
			Board ladderb1 = *b;
		//return spaces0.size()*10+spaces1.size();;
		//Board ladderb = *b;
			move(&ladderb1,libz1,un_col);
			move(&ladderb1,libz0,color);
			return check_ladder_dataunsafe(&ladderb1,ladderb1.z_to_groupid[libz0],depth-1);

	}

	if (escapez_flag0==1&&escapez_flag1==0)
	{
			Board ladderb0 = *b;
		//return spaces0.size()*10+spaces1.size();;
		//Board ladderb = *b;
			move(&ladderb0,libz0,un_col);
			move(&ladderb0,libz1,color);
			return check_ladder_dataunsafe(&ladderb0,ladderb0.z_to_groupid[libz1],depth-1);
	}
	if (escapez_flag0==0&&escapez_flag1==0){
		Board ladderb0 = *b;
		move(&ladderb0,libz0,un_col);
		move(&ladderb0,libz1,color);
		Board ladderb1 = *b;
		move(&ladderb1,libz1,un_col);
		move(&ladderb1,libz0,color);
		//return b->z_to_groupid[libz0]*10;
		//return check_ladder_3move(&ladderb0,b->z_to_groupid[libz1],depth-1);
		//return check_ladder_3move(&ladderb1,b->z_to_groupid[libz0],1);
		return min(check_ladder_dataunsafe(&ladderb0,ladderb0.z_to_groupid[libz1],depth-1),
					check_ladder_dataunsafe(&ladderb1,ladderb1.z_to_groupid[libz0],depth-1));
	}
	return 0;
}


int check_ladder_dataunsafe(Board *b,int gid,int depth)
{
	if (depth == 0)
	{
		return 0;
	}
	//return b->group[gid].lib_z.size();
	if (b->group[gid].lib_z.size()>2)
	{
		return 1;
	}	
	if (b->group[gid].lib_z.size()<2)
	{
		return -1;
	}
	

	//if(b->group[b->group[gid].neighbor_opp[0]].lib_z.size()==1)
	//return b->group[gid].neighbor_opp[0]*10+b->group[gid].neighbor_opp[1];
	for (int i = 0; i < b->group[gid].neighbor_opp.size(); ++i)
	{
	  	if(b->group[b->group[gid].neighbor_opp[i]].lib_z.size()==1)
	  		return 1;
	}


	int color = b->group[gid].color ;
	int un_col = flip_color(color);
	int libz0 = b->group[gid].lib_z[0];
	int libz1 = b->group[gid].lib_z[1];
	int escapez_flag0 = 0;//by moving this libz ,1 escape,2
	int escapez_flag1 = 0;//by moving this libz ,1 escape,2

	std::vector<int> oppgroups0;//id
	std::vector<int> frigroups0;//id
	std::vector<int> spaces0;//4方向の空白のz
	std::vector<int> walls0;//4方向の盤外のz



	for (int i=0;i<4;i++){
		int z = libz0+b->dir4[i];
		int c = b->data[z]; // 石の色
		if ( c == 0 && z!=libz1) 
			spaces0.push_back(z);
		else if ( c == color ) 
			frigroups0.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == un_col )
			oppgroups0.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == 3 ) 
			walls0.push_back(z);
	}




	
	unique_int_vec(&frigroups0);
	unique_int_vec(&oppgroups0);
	del_item_int_vec(&frigroups0,gid);	






	//if (frigroups0.size()>0)
	for (int m = 0; m < frigroups0.size(); ++m)
	{
		for (int i = 0; i < b->group[frigroups0[m]].lib_z.size(); ++i)
		{
			if (b->group[frigroups0[m]].lib_z[i]!=libz0)
			{
				spaces0.push_back(b->group[frigroups0[m]].lib_z[i]);
			}
			
		}

	}
	unique_int_vec(&spaces0);

	if (spaces0.size()>2)
	{
		escapez_flag0 = 1;
	}else if (spaces0.size()==2)
	{
		escapez_flag0 = 0;
	}else escapez_flag0 = -1;


	//if (oppgroups0.size()>0)
	for (int m = 0; m < oppgroups0.size(); ++m)
	{	
		if (b->group[oppgroups0[m]].lib_z.size()==2&&spaces0.size()==2)
		{
			escapez_flag0 = 1;
		}
		if (b->group[oppgroups0[m]].lib_z.size()==1&&b->group[oppgroups0[0]].lib_z[0]!=libz0)
		{
			escapez_flag0 = 1;
		}
	}

	if (escapez_flag0 == -1)
		return -1;





	std::vector<int> oppgroups1;//id
	std::vector<int> frigroups1;//id
	std::vector<int> spaces1;//4方向の空白のz
	std::vector<int> walls1;//4方向の盤外のz

	for (int i=0;i<4;i++){
		int z = libz1+b->dir4[i];
		int c = b->data[z]; // 石の色
		if ( c == 0 && z!=libz0 ) 
			spaces1.push_back(z);
		else if ( c == color ) 
			frigroups1.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == un_col ) 
			oppgroups1.push_back(get_rgroup_id(b,b->z_to_groupid[z]));
		else if ( c == 3 ) 
			walls1.push_back(z);
	}

	unique_int_vec(&frigroups1);
	unique_int_vec(&oppgroups1);
	del_item_int_vec(&frigroups1,gid);




	//if (frigroups1.size()>0)
	for (int m = 0; m < frigroups1.size(); ++m)
	{
		for (int i = 0; i < b->group[frigroups1[m]].lib_z.size(); ++i)
		{
			if (b->group[frigroups1[m]].lib_z[i]!=libz1)
				spaces1.push_back(b->group[frigroups1[m]].lib_z[i]);
		}
	}
	unique_int_vec(&spaces1);

	if (spaces1.size()>2)
	{
		escapez_flag1 = 1;
	}else if (spaces1.size()==2)
	{
		escapez_flag1 = 0;
	}else escapez_flag1 = -1;

		//if (oppgroups1.size()>0)
	for (int m = 0; m < oppgroups1.size(); ++m)
	{	
		if (b->group[oppgroups1[m]].lib_z.size()==2&&spaces1.size()==2)
		{
			escapez_flag1 = 1;
		}
		if (b->group[oppgroups1[m]].lib_z.size()==1&&b->group[oppgroups1[m]].lib_z[0]!=libz1)
		{
			escapez_flag1 = 1;
		}
	}


	if (escapez_flag0==-1||escapez_flag1==-1)
	{
		return -1;
	}	
	if (escapez_flag0==1&&escapez_flag1==1)
	{
		return 1;
	}


	if (escapez_flag0==0&&escapez_flag1==1)
	{
			//Board ladderb1 = *b;
		//return spaces0.size()*10+spaces1.size();;
		//Board ladderb = *b;
		if (depth-1 == 0)
		{
			return 0;
		}

			move(b,libz1,un_col);
			move(b,libz0,color);
			return check_ladder_dataunsafe(b,b->z_to_groupid[libz0],depth-1);

	}

	if (escapez_flag0==1&&escapez_flag1==0)
	{
			//Board ladderb0 = *b;
		//return spaces0.size()*10+spaces1.size();;
		//Board ladderb = *b;
		if (depth-1 == 0)
		{
			return 0;
		}
			move(b,libz0,un_col);
			move(b,libz1,color);
			return check_ladder_dataunsafe(b,b->z_to_groupid[libz1],depth-1);
	}
	if (escapez_flag0==0&&escapez_flag1==0){
		Board ladderb1 = *b;
		move(&ladderb1,libz1,un_col);
		move(&ladderb1,libz0,color);
		if (depth-1 == 0)
		{
			return 0;
		}
		move(b,libz0,un_col);
		move(b,libz1,color);
		//return b->z_to_groupid[libz0]*10;
		//return check_ladder_3move(&ladderb0,b->z_to_groupid[libz1],depth-1);
		//return check_ladder_3move(&ladderb1,b->z_to_groupid[libz0],1);
		return min(check_ladder_dataunsafe(b,b->z_to_groupid[libz1],depth-1),
					check_ladder_dataunsafe(&ladderb1,ladderb1.z_to_groupid[libz0],depth-1));
	}
	return 0;
}
