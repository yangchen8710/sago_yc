#include <stdint.h>
#include <boost/unordered_map.hpp>

typedef boost::unordered_map<uint64_t, uint64_t> map_u64u64;
typedef boost::unordered_map<uint64_t, int> map_u64int;

void unique_int_vec(std::vector<int> *v);

void del_item_int_vec(std::vector<int> *v,int i);

int flip_color(int col);

static inline uint32_t random32();

uint64_t random64();

template <typename T> std::string to_string(const T& arg);