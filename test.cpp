#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <assert.h>
#include <iostream>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>


using namespace std;

#include "board.h"
#include "feature.h"
#include "common.h"

int *mcnum_b;
#include "playout.h"
#include "ladder.h"


void getgtpz(int z,char *sxy)
{
  if ( z==0 ) return;
  int y = z / 11;    
  int x = z - y*11; 

  if(x<9){
    sxy[0] = ('A'+x-1);
  }
  else{
    sxy[0] = 'A'+x;
  }

  if(y>9){
    sxy[1] = ('0'+y/10);
    sxy[2] = ('0'+(y%10));
    sxy[3]='\0';
  }
  else {
    sxy[1]=('0'+y);
    sxy[2]='\0';
  }
}


void send_gtp(const char *fmt, ...)
{
 va_list ap;

 va_start(ap, fmt);
 vfprintf( stdout, fmt, ap );  
 va_end(ap);
}

int main()
{
    uint64_t shash[4][200];
    uint64_t positionhash[9][500];

    //FILE *filepr = fopen("uint64list.dat","r");

    for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 121; ++j)
        shash[i][j] = random64();
        //fscanf(filepr, "%llu", &shash[i][j]);
    //shash[tc][(5+ty)*11+(5+tx)]
    


    srand((unsigned)time(NULL));
    Board *b = new Board;
    init_board(b,19,6.5);
    init_board_data(b);
    mcnum_b=(int *)malloc(sizeof(int)*b->bmax);

    //char coordc[]={'A','B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','S','T'};



    for (int i = 0; i < 9; ++i)
    for (int j = 0; j < 500; ++j)

        {
            positionhash[i][j] = 0;
        }

        int x,y;
        //send_gtp("=  ");

        for (y=0; y<b->bsize; y++) 
            for (x=0; x<b->bsize; x++) {
                uint64_t minihash4r[9][4];
                for (int mk = 0; mk < 9; ++mk)
                for (int ml = 0; ml < 4; ++ml)
                {
                    minihash4r[mk][ml] =0;
                }

                for (int dy = 0; dy < 6; ++dy)
                    for (int dx = 0; dx < 6; ++dx){
                        if (dy==0&&dx==0)
                        {
                            continue;
                        }

                        int dis = dx+dy+max(dx,dy);
                        //printf("%d %d %d\n",dx,dy,dis );
                        if (dis<11)
                        {
                            int tc;
                            int tx,ty;
                            tx = dx;
                            ty = dy;
                            if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
                            {
                                //d4z[0] = get_z(b,x,y);
                                tc = b->data[get_z(b,x+tx,y+ty)];
                            }else tc =0;
                            if (tc!=0)
                            {
                                minihash4r[dis-2][0]=minihash4r[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
                                minihash4r[dis-2][1]=minihash4r[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
                                minihash4r[dis-2][2]=minihash4r[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
                                minihash4r[dis-2][3]=minihash4r[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
                            }

                            tx = -dx;
                            ty = dy;
                            if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
                            {
                                //d4z[0] = get_z(b,x,y);
                                tc = b->data[get_z(b,x+tx,y+ty)];
                                
                            }else tc =0;
                            if (tc!=0)
                            {
                                //printf("%d\n",tc);
                                minihash4r[dis-2][0]=minihash4r[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
                                minihash4r[dis-2][1]=minihash4r[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
                                minihash4r[dis-2][2]=minihash4r[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
                                minihash4r[dis-2][3]=minihash4r[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
                            }

                            tx = dx;
                            ty = -dy;
                            if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
                            {
                                //d4z[0] = get_z(b,x,y);
                                //printf("x= %d,y=%d,z=%d\n",x+tx,y+ty, get_z(b,x+tx,y+ty));
                                tc = b->data[get_z(b,x+tx,y+ty)];
                            }else tc =0;
                            if (tc!=0)
                            {

                                minihash4r[dis-2][0]=minihash4r[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
                                minihash4r[dis-2][1]=minihash4r[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
                                minihash4r[dis-2][2]=minihash4r[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
                                minihash4r[dis-2][3]=minihash4r[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
                            }

                            tx = -dx;
                            ty = -dy;
                            if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
                            {
                                //d4z[0] = get_z(b,x,y);
                                tc = b->data[get_z(b,x+tx,y+ty)];
                            }else tc =0;
                            if (tc!=0)
                            {
                                minihash4r[dis-2][0]=minihash4r[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
                                minihash4r[dis-2][1]=minihash4r[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
                                minihash4r[dis-2][2]=minihash4r[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
                                minihash4r[dis-2][3]=minihash4r[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
                            }
                        }
            }
        }

    return 0;
    //*/

    /*
    char str[256];
    int gtpz,gtpx,gtpy;
    initboard();
    gtpz = select_best_uct(1);
    move(gtpz,1);
    print_board();
    gtpz = select_best_uct(2);
    move(gtpz,2);
    print_board();
    */

}


