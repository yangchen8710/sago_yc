#include <stdio.h>
#include <malloc.h>
#include <time.h>
#include <stdlib.h>

#include "board.h"
#include "playout.h"
#include "common.h"

int count_mco(Board *b)
{
  int score = 0;
  int kind[3] = {0,0,0};  // 盤上に残ってる石数
  //kind[0] = kind[1] = kind[2] = 0;
  int x,y,i;
  for (y=0;y<b->bsize;y++) for (x=0;x<b->bsize;x++) {
    int z = get_z(b,x,y);
    int c = b->data[z];
    kind[c]++;
    if (c == 1)
      mcnum_b[z]++;
    if ( c != 0 ) continue;

  int mk[4];  // 空点は4方向の石を種類別に数える
  mk[1] = mk[2] = 0;  
  for (i=0;i<4;i++) 
    mk[ b->data[z+b->dir4[i]] ]++;
  if ( mk[1] && mk[2]==0 ) {
    score++; // 同色だけに囲まれていれば地
    mcnum_b[z]++;
  }
  if ( mk[2] && mk[1]==0 ) score--;
  }
  score += kind[1] - kind[2];
 
  double final_score = score - b->komi;
  int win = 0;
  if ( final_score > 0 ) win = 1;
  return win;
}

int playout_mco(Board *b, int turn_color)
{
  //srand( (unsigned)time( NULL ) );
  int color = turn_color;
  int before_z = 0; // 1手前の手
  int loop;
  int loop_max = b->bmax + 200; // 最大でも300手程度まで。3コウ対策
      int *kouho=(int *)malloc(sizeof(int)*b->bmax);
  for (loop=0; loop<loop_max; loop++) {
    // すべての空点を着手候補にする

    int kouho_num = 0;
    int x,y;

    for (y=0;y<b->bsize;y++) for (x=0;x<b->bsize;x++) {


      int z = get_z(b,x,y);
      if ( b->data[z] != 0 ) continue;
      kouho[kouho_num++] = z;
    }
    int z,r = 0;
    for (;;) {


      if ( kouho_num == 0 ) {
        z = 0;
      } else {
        r = rand() % kouho_num;   // 乱数で1手選ぶ
        z = kouho[r];
      }
      int err = move(b,z,color);
      if ( err >= 0 ) 
        {
          break;
        }
      kouho[r] = kouho[kouho_num-1];  // エラーなので削除
      kouho_num--;
    }
    if ( z == 0 && before_z == 0 ) break; // 連続パス
    before_z = z;
//    print_board();
//    printf("loop=%d,z=%d,c=%d,kouho_num=%d,ko_z=%d\n",loop,get81(z),color,kouho_num,get81(ko_z));
    color = flip_color(color);
    
  }
  free(kouho);
  return count_mco(b);
}