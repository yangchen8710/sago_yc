#include <vector>
#include <stdint.h>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <stdio.h>
using namespace std;
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "board.h"
#include "common.h"

#include "pattern.h"

void init_board(Board *b, int bsize,float komi)
{
	b->last_move1 = 0;
	b->last_move2 = 0;
	b->bsize = bsize;
	b->bwidth = bsize+2;
	b->bmax = b->bwidth*b->bwidth;
	b->dir4[0] = +1;
	b->dir4[1] = -1;
	b->dir4[2] = +b->bwidth;
	b->dir4[3] = -b->bwidth;
	b->data.resize(b->bmax);	
	b->z_to_groupid.resize(b->bmax);
	b->komi = komi;
	b->ko_z = 0;
	b->lm1c = 2;
}

void init_board_data(Board *b)
{
    for (int y=0;y<b->bwidth;y++)
    for(int x=0;x<b->bwidth;x++)
    {
        if(x==0||y==0||x==(b->bwidth-1)||y==(b->bwidth-1)){
        	b->data[y*b->bwidth+x]=3;
        	b->z_to_groupid[y*b->bwidth+x]=-2;//offboard's group_id = -2
        }
        else {
        	b->data[y*b->bwidth+x]=0;
        	b->z_to_groupid[y*b->bwidth+x]=-1;//init group_id =-1
        }
        
    }
    for (int y=0; y<b->bsize; y++) 
	    for(int x=0; x<b->bsize; x++) 
	        pattern_hashxy(b,x,y);
}

void print_board(Board *b)
{
    int x,y;
    char str[3]={'.','x','o'};
    //printf("%d %d %d %d\n", b->dir4[0], b->dir4[1], b->dir4[2], b->dir4[3] );
    cout <<  "   ";
    //for (x=0; x<b->bsize; x++) {cout <<(x+1) << " ";}
    cout << endl;
    for (y=0; y<b->bsize; y++) {
        //cout<<setprecision(2)<<(y+1) << ": ";
        for (x=0; x<b->bsize; x++) {
            cout << (str[b->data[get_z(b,x,y)]]) << " ";
        }
        cout << endl;
    }
}

int get_x(Board *b,int z)
{
	return z%b->bwidth-1;
}

int get_y(Board *b,int z)
{
	return z/b->bwidth-1;
}

int get_z(Board *b,int x,int y)
{
	return (y+1)*b->bwidth + (x+1);
}

int get_rgroup_id(Board *b,int gid){

	while (b->group[gid].id != gid)
	{
		gid = b->group[gid].id;
	}
	return gid;
}

void capture_group(Board *b,int gid,std::vector<int> *changed_z){
	for (int i = 0; i < b->group[gid].neighbor_opp.size(); ++i)
	{
		int oppid = b->group[gid].neighbor_opp[i];
		if (b->group[oppid].neighbor_opp.size()==0)
		{
			//printf("%d\n", 1);
		}else
		del_item_int_vec(&b->group[oppid].neighbor_opp,gid);
	}


	int color = b->group[gid].color;
	int un_col = flip_color(color);

	for (int m = 0; m < b->group[gid].member_z.size(); ++m)
	{

		int tz = b->group[gid].member_z[m];
		
		std::vector<int> oppgroups;
		for (int i=0;i<4;i++){
			int z = tz+b->dir4[i];
			if (b->data[z]==un_col)
			{
				oppgroups.push_back(b->z_to_groupid[z]);
			}
		}

		unique_int_vec(&oppgroups);


		if (oppgroups.size()==1)
		{
			 b->group[oppgroups[0]].lib_z.push_back(tz);
		}else{
			for (int i = 0; i < oppgroups.size(); ++i)
			{
				b->group[oppgroups[i]].lib_z.push_back(tz);
			}
		}



		b->data[tz] = 0;
		changed_z->push_back(tz);
		b->z_to_groupid[tz] = -1;
	}
	std::vector<int> oppgroups;

	for (int i = 0; i < b->group[gid].neighbor_opp.size(); ++i)
		oppgroups.push_back(b->group[gid].neighbor_opp[i]);
	unique_int_vec(&oppgroups);
	for (int i = 0; i < oppgroups.size(); ++i)
	{
		unique_int_vec(&b->group[oppgroups[i]].lib_z);
	}



}


void merge_groups(Board *b,int gid1,int gid2,int tz){


	for (int i = 0; i < b->group[gid2].lib_z.size(); ++i)
	{
		int libz = b->group[gid2].lib_z[i];
		if (tz != libz)
		{
			b->group[gid1].lib_z.push_back(libz);
		}
		
	}
	for (int i = 0; i < b->group[gid2].member_z.size(); ++i)
	{
		b->group[gid1].member_z.push_back(b->group[gid2].member_z[i]);
		b->z_to_groupid[b->group[gid2].member_z[i]] = gid1;
	}
	for (int i = 0; i < b->group[gid2].neighbor_opp.size(); ++i)
	{
		int oppid = b->group[gid2].neighbor_opp[i];
		b->group[gid1].neighbor_opp.push_back(oppid);

		vector<int>::iterator  it = find(b->group[oppid].neighbor_opp.begin(),b->group[oppid].neighbor_opp.end(),gid2);
		*it = gid1;

	    unique_int_vec(&b->group[oppid].neighbor_opp);

	}
	b->group[gid2].id = gid1;

}


int move(Board *b,int tz,int color)//-1コウ,-2既に石がある,-3自殺手,-4眼(ルール違反ではない). 0パス,1captured,2other
{  
	if ( tz == 0 ) { b->ko_z = 0; return 0; }  // パスの場合
	if ( tz == b->ko_z) return -1; // コウ
	if ( b->data[tz] != 0) return -2; // 既に石がある

	std::vector<int> oppgroups;//id
	std::vector<int> frigroups;//id
	std::vector<int> spaces;//4方向の空白のz
	std::vector<int> walls;//4方向の盤外のz

	int un_col = flip_color(color); // 相手の石の色

	int fri_safe = 0;  // ダメ2以上で安全な味方
	//int opp_take_flag = 0;   // 石 取れる flag
	int take_sum = 0;	//// 取れる石
	int ko_maybe = 0;    // コウになるかもしれない場所
	int i;

	for (i=0;i<4;i++){
		int z = tz+b->dir4[i];
		int c = b->data[z]; // 石の色
		if ( c == 0 ) {
			spaces.push_back(z);
			continue;
		}
		if ( c == color ) {
			int gid = get_rgroup_id(b,b->z_to_groupid[z]);
			frigroups.push_back(gid);
			if(b->group[gid].lib_z.size()>1)
				fri_safe++;
			continue;
		}
		if ( c == un_col ) {
			int gid = get_rgroup_id(b,b->z_to_groupid[z]);
			oppgroups.push_back(gid);
			if(b->group[gid].lib_z.size()==1){
				take_sum += b->group[gid].member_z.size();
				spaces.push_back(z);
				ko_maybe = z;
			}
			continue;
		}
		if ( c == 3 ) {
			walls.push_back(z);
			continue;
		}

	}

	if ( fri_safe == 0 && spaces.size() == 0  ) return -3; // 自殺手
	if ( walls.size() + fri_safe == 4 ) return -4; // 眼(ルール違反ではない)
	unique_int_vec(&frigroups);
	unique_int_vec(&oppgroups);

    //vector<int>::iterator iter;  /.[[[[[-[[=[=]]]]]]]]
    std::vector<int> changed_z;
    if(take_sum>0){
    	for (int i = 0; i < oppgroups.size(); ++i)
    	{
    			if (b->group[oppgroups[i]].lib_z.size()==1)
					capture_group(b,oppgroups[i],&changed_z);

    	}
	}


	
    if (frigroups.size()==0){
    	//create new group
    	GROUP ng;
    	ng.id = b->group.size();
    	ng.color = color;
    	ng.member_z.push_back(tz);
    	for (int i = 0; i < spaces.size(); ++i)
    	{
    		ng.lib_z.push_back(spaces[i]);
    	}

			
		for (int i = 0; i < oppgroups.size(); ++i)
		{//del opp group lib at tz ; ng add neighbor opp
			if(b->group[oppgroups[i]].lib_z.size()==1) continue;//pass captured opp group
			

			del_item_int_vec(&b->group[oppgroups[i]].lib_z,tz);
		

	    	b->group[oppgroups[i]].neighbor_opp.push_back(ng.id);

	    	ng.neighbor_opp.push_back(oppgroups[i]);

		}

			

		b->group.push_back(ng);
    	b->z_to_groupid[tz] = ng.id;
    	

    }else{//add tz to frigroup[0]
    	int gid = frigroups[0];

    	//del frigroup[0]'s lib at tz
    	del_item_int_vec(&b->group[gid].lib_z,tz);

    	b->group[gid].member_z.push_back(tz);

    	for (int i = 0; i < spaces.size(); ++i)
    	{
    		b->group[gid].lib_z.push_back(spaces[i]);
    	}
    	unique_int_vec(&b->group[gid].lib_z);

    	b->z_to_groupid[tz] = gid;

    	for (int i = 0; i < oppgroups.size(); ++i)
		{//del opp lib at tz,// add neighbor opp to frigroup[0]
			if(b->group[oppgroups[i]].lib_z.size()==1) continue;
			del_item_int_vec(&b->group[oppgroups[i]].lib_z,tz);

			b->group[oppgroups[i]].neighbor_opp.push_back(gid);
			unique_int_vec(&b->group[oppgroups[i]].neighbor_opp);

			b->group[gid].neighbor_opp.push_back(oppgroups[i]);
			

		}
		//unique_int_vec(&b->group[oppgroups[i]].neighbor_opp);


    	//merge frigroups to frigroup[0]
    	for (int o = 1; o < frigroups.size(); ++o)
    	{
    		merge_groups(b,frigroups[0],frigroups[o],tz);

    	}
    	//for (int o = 0; o < b->group[frigroups[0]].neighbor_opp.size(); ++o)unique_int_vec(&b->group[b->group[frigroups[0]].neighbor_opp[o]].neighbor_opp);
    	



    	for (int i = 0; i < oppgroups.size(); ++i)
		{// add neighbor opp to frigroup[0]
			    			
			//pass captured opp grou
			if(b->group[oppgroups[i]].lib_z.size()==1) {continue;}
			

			b->group[gid].neighbor_opp.push_back(oppgroups[i]);
			b->group[oppgroups[i]].neighbor_opp.push_back(gid);


			unique_int_vec(&b->group[oppgroups[i]].neighbor_opp);
		
		}
		    
		//del same lib at frigroup[0]
		unique_int_vec(&b->group[gid].lib_z);
    	//del same neighbor_opp at frigroup[0]
    	unique_int_vec(&b->group[gid].neighbor_opp);

    }
    b->data[tz] = color;  // 石を置く
    changed_z.push_back(tz);
    // pattern_areahash(b,&changed_z);

    b->last_move2 = b->last_move1;
    b->last_move1 = tz;
    b->lm1c = color;

    //if (frigroups.size()>0)ko_maybe = 0;

    if (take_sum==1 && frigroups.size()==0 && b->group[b->z_to_groupid[tz]].lib_z.size()==1)
    	{b->ko_z = ko_maybe;}else 
    b->ko_z = ko_maybe;

    
    return 2;
}