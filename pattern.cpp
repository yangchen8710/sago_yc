#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <stdio.h>
#include <math.h>
#include <boost/unordered_map.hpp>
using namespace std;

#include "common.h"
#include "board.h"
#include "ladder.h"
#include "pattern.h"

void pattern_areahash(Board *b,std::vector<int> *changed_z) {

	std::vector<int> updatez;
	for (vector<int>::iterator  it = changed_z->begin(); it!=changed_z->end(); ++it){
		int x = get_x(b,*it);
		int y = get_y(b,*it);
		for (int dy = 0; dy < 6; ++dy)
		for (int dx = 0; dx < 6; ++dx)
		{
			int tx = dx;
			int ty = dy;
			if (x+tx<19&&y+ty<19&&x+tx>-1&&y+ty>-1)
				updatez.push_back(get_z(b,x+tx,y+ty));
			tx = -dx;
			ty = dy;
			if (x+tx<19&&y+ty<19&&x+tx>-1&&y+ty>-1)
				updatez.push_back(get_z(b,x+tx,y+ty));
			tx = dx;
			ty = -dy;
			if (x+tx<19&&y+ty<19&&x+tx>-1&&y+ty>-1)
				updatez.push_back(get_z(b,x+tx,y+ty));
			tx = -dx;
			ty = -dy;
			if (x+tx<19&&y+ty<19&&x+tx>-1&&y+ty>-1)
				updatez.push_back(get_z(b,x+tx,y+ty));
		}
	}

	unique_int_vec(&updatez);
	for (int i = 0; i < updatez.size(); ++i)
	{
		//printf("updatez=%d\n",updatez[i] );
		pattern_hashxy(b,get_x(b,updatez[i]),get_y(b,updatez[i]));
	}



}
void pattern_hashxy(Board *b,int x,int y) {
	int xyz = get_z(b,x,y);
	if (b->data[xyz])
	{
		for (int i = 0; i < 9; ++i){
			b->positionhashw[i][xyz] = 0;
			b->positionhashb[i][xyz] = 0;
		}
		return;
	}
	uint64_t subhash8b[9][8];
	uint64_t subhash8w[9][8];
    for (int mk = 0; mk < 9; ++mk)
	for (int ml = 0; ml < 8; ++ml){
				subhash8b[mk][ml]=0;
				subhash8w[mk][ml]=0;
	}

	for (int dy = 0; dy < 6; ++dy)
	for (int dx = 0; dx < 6; ++dx){
		if (dx==0&&dy==0)continue;
		int dis = dx+dy+max(dx,dy);
		if (dis<11)
		{
			int tc;
			int tx,ty;


			tx = dx;
			ty = dy;
			if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
				tc = b->data[get_z(b,x+tx,y+ty)];
			else tc =0;
			if(tc!=0){
				subhash8b[dis-2][0]=subhash8b[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
				subhash8b[dis-2][1]=subhash8b[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
				subhash8b[dis-2][2]=subhash8b[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
				subhash8b[dis-2][3]=subhash8b[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
				subhash8b[dis-2][4]=subhash8b[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
				subhash8b[dis-2][5]=subhash8b[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
				subhash8b[dis-2][6]=subhash8b[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
				subhash8b[dis-2][7]=subhash8b[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];

				if (tc==1||tc==2)
					tc = 3-tc;

				subhash8w[dis-2][0]=subhash8w[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
				subhash8w[dis-2][1]=subhash8w[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
				subhash8w[dis-2][2]=subhash8w[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
				subhash8w[dis-2][3]=subhash8w[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
				subhash8w[dis-2][4]=subhash8w[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
				subhash8w[dis-2][5]=subhash8w[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
				subhash8w[dis-2][6]=subhash8w[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
				subhash8w[dis-2][7]=subhash8w[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];


			}

			if(dx){
				tx = -dx;
				ty = dy;
				if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
					tc = b->data[get_z(b,x+tx,y+ty)];
				else tc =0;
				if (tc!=0){
					subhash8b[dis-2][0]=subhash8b[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
					subhash8b[dis-2][1]=subhash8b[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
					subhash8b[dis-2][2]=subhash8b[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
					subhash8b[dis-2][3]=subhash8b[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
					subhash8b[dis-2][4]=subhash8b[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
					subhash8b[dis-2][5]=subhash8b[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
					subhash8b[dis-2][6]=subhash8b[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
					subhash8b[dis-2][7]=subhash8b[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];

					if (tc==1||tc==2)
						tc = 3-tc;

					subhash8w[dis-2][0]=subhash8w[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
					subhash8w[dis-2][1]=subhash8w[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
					subhash8w[dis-2][2]=subhash8w[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
					subhash8w[dis-2][3]=subhash8w[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
					subhash8w[dis-2][4]=subhash8w[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
					subhash8w[dis-2][5]=subhash8w[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
					subhash8w[dis-2][6]=subhash8w[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
					subhash8w[dis-2][7]=subhash8w[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];
				}
			}

			if(dy){
				tx = dx;
				ty = -dy;
				if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
					tc = b->data[get_z(b,x+tx,y+ty)];
				else tc =0;
				if (tc!=0){
					subhash8b[dis-2][0]=subhash8b[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
					subhash8b[dis-2][1]=subhash8b[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
					subhash8b[dis-2][2]=subhash8b[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
					subhash8b[dis-2][3]=subhash8b[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
					subhash8b[dis-2][4]=subhash8b[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
					subhash8b[dis-2][5]=subhash8b[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
					subhash8b[dis-2][6]=subhash8b[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
					subhash8b[dis-2][7]=subhash8b[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];

					if (tc==1||tc==2)
						tc = 3-tc;

					subhash8w[dis-2][0]=subhash8w[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
					subhash8w[dis-2][1]=subhash8w[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
					subhash8w[dis-2][2]=subhash8w[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
					subhash8w[dis-2][3]=subhash8w[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
					subhash8w[dis-2][4]=subhash8w[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
					subhash8w[dis-2][5]=subhash8w[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
					subhash8w[dis-2][6]=subhash8w[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
					subhash8w[dis-2][7]=subhash8w[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];
				}
			}


			if (dx&&dy){
				tx = -dx;
				ty = -dy;
				if (x+tx<20&&y+ty<20&&x+tx>-2&&y+ty>-2)
					tc = b->data[get_z(b,x+tx,y+ty)];
				else tc =0;
				if (tc!=0){
					subhash8b[dis-2][0]=subhash8b[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
					subhash8b[dis-2][1]=subhash8b[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
					subhash8b[dis-2][2]=subhash8b[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
					subhash8b[dis-2][3]=subhash8b[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
					subhash8b[dis-2][4]=subhash8b[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
					subhash8b[dis-2][5]=subhash8b[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
					subhash8b[dis-2][6]=subhash8b[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
					subhash8b[dis-2][7]=subhash8b[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];

					if (tc==1||tc==2)
						tc = 3-tc;

					subhash8w[dis-2][0]=subhash8w[dis-2][0]^shash[tc][(5+ty)*11+(5+tx)];
					subhash8w[dis-2][1]=subhash8w[dis-2][1]^shash[tc][(5-tx)*11+(5+ty)];
					subhash8w[dis-2][2]=subhash8w[dis-2][2]^shash[tc][(5-ty)*11+(5-tx)];
					subhash8w[dis-2][3]=subhash8w[dis-2][3]^shash[tc][(5+tx)*11+(5-ty)];
					subhash8w[dis-2][4]=subhash8w[dis-2][4]^shash[tc][(5+ty)*11+(5-tx)];
					subhash8w[dis-2][5]=subhash8w[dis-2][5]^shash[tc][(5-tx)*11+(5-ty)];
					subhash8w[dis-2][6]=subhash8w[dis-2][6]^shash[tc][(5-ty)*11+(5+tx)];
					subhash8w[dis-2][7]=subhash8w[dis-2][7]^shash[tc][(5+tx)*11+(5+ty)];
				}
			}
		}
}

for (int m = 0; m < 8; ++m)
for (int n = 0; n < 8; ++n){
	subhash8b[n+1][m] = subhash8b[n+1][m]^subhash8b[n][m];
	subhash8w[n+1][m] = subhash8w[n+1][m]^subhash8w[n][m];
}


uint64_t tempb[9],tempw[9];
for (int i = 0; i < 9; ++i){
	tempb[i] = subhash8b[i][0];
	tempw[i] = subhash8w[i][0];
}

for (int n = 1; n < 9; ++n)
for (int m = 1; m < 8; ++m){
	if (tempb[n]>subhash8b[n][m])
		tempb[n]=subhash8b[n][m];
	if (tempw[n]>subhash8w[n][m])
		tempw[n]=subhash8w[n][m];
}

for (int n = 0; n < 9; ++n)
for (int m = 0; m < 8; ++m){
	if (b->lm1c==2&&!pattern_8to1[n][subhash8b[n][m]])
		pattern_8to1[n][subhash8b[n][m]]=tempb[n];
	if (b->lm1c==1&&!pattern_8to1[n][subhash8w[n][m]])
		pattern_8to1[n][subhash8w[n][m]]=tempw[n];
}

for (int i = 0; i < 9; ++i){
	b->positionhashb[i][get_z(b,x,y)] = tempb[i];
	b->positionhashw[i][get_z(b,x,y)] = tempw[i];
}
}
