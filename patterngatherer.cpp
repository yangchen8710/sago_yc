#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <boost/unordered_map.hpp>
#include <boost/lexical_cast.hpp> 

#define __STDC_FORMAT_MACROS
#include <inttypes.h>


using namespace std;
#include "board.h"
#include "feature.h"

#include "common.h"
map_u64u64 pattern_8to1[8];
map_u64int pattern_times[9];
int *mcnum_b;
#include "playout.h"
#include "ladder.h"
uint64_t shash[4][121];
#include "pattern.h"



typedef struct movelog {
	int c;
	int x;
	int y;
}MOVELOG;

int main(int argc, char const *argv[])
{


	FILE *filepr = fopen("uint64list.dat","r");
	for (int i = 0; i < 4; ++i)
	for (int j = 0; j < 121; ++j)
		fscanf(filepr, "%llu", &shash[i][j]);
		//shash[i][j] = random64();
std::string s = boost::lexical_cast<string>(10); 

for (int sgfi = 10; sgfi < 16; ++sgfi)
{
	string s = boost::lexical_cast<string>(sgfi); 
	s +=".sgf";
	char ch[20]; 
	strcpy(ch,s.c_str());

	ifstream fin(ch,ios::in);
	if(!fin){
		cout<<"File open error!\n";
		return 0;
	}
	char c,d,e;
	std::vector<MOVELOG> movelog;

	int sgfdepth = 0 ;

	while((c=fin.get())!=EOF){
		if (c=='(')
			sgfdepth++;
		if (c==')')
			sgfdepth--;
		if (sgfdepth==1&&c==';'){
			d=fin.get();e=fin.get();
			if (d=='B'&&e=='[')
			{
				MOVELOG ml;
				ml.c=1;ml.x=fin.get()-'a';ml.y=fin.get()-'a';
				movelog.push_back(ml); //cout<<a<<fin.get()-'a'<<" "<<fin.get()-'a'<<endl;
			}
			if (d=='W'&&e=='[')
			{
				MOVELOG ml;
				ml.c=2;ml.x=fin.get()-'a';ml.y=fin.get()-'a';
				movelog.push_back(ml);
				//cout<<a<<fin.get()-'a'<<" "<<fin.get()-'a'<<endl;
			}
		}
		//c=fin.get();
  	}
  	fin.close();




	int x,y;
	Board *b = new Board;
	init_board(b,19,6.5);
	init_board_data(b);

  	for (int i = 0; i < movelog.size(); ++i){
  		for (int y=0; y<b->bsize; y++) 
	    for(int x=0; x<b->bsize; x++) 
	        pattern_hashxy(b,x,y);

	    map_u64int temp[9];
	    for (int y=0; y<b->bsize; y++) 
	    for(int x=0; x<b->bsize; x++) 
	    	for (int ni = 1; ni < 9; ++ni)
	    	{
	    		//printf("%d %d\n", x,y);
	    		uint64_t thash ;
	    		if (b->lm1c==1)
	    			thash=b->positionhashw[ni][get_z(b,x,y)];
	    		if (b->lm1c==2)
	    			thash=b->positionhashb[ni][get_z(b,x,y)];
	    		if (thash&&!temp[ni][thash])
	    		{
	    			temp[ni][thash]=1;
	    			//printf("%d\n", pattern_times[ni][thash]);
	    			if (!pattern_times[ni][thash])
	    			{
	    				pattern_times[ni][thash]=1;
	    			}else
	    				pattern_times[ni][thash]++;
	    		}
	    	}
  		move(b,get_z(b,movelog[i].x,movelog[i].y),movelog[i].c);
  	}
}

  	//pattern_times[0][1] = 3;  
  	//pattern_times[0][2] = 4;  

  	for (map_u64int::const_iterator it = pattern_times[3].begin();
       	it != pattern_times[3].end(); ++it) {
  		if(it->second>200)
    	cout << it->first << ", " << it->second << endl;
  	}



	return 0;
}
