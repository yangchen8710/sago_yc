#include <vector>
#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;

#include "common.h"
#include "board.h"
#include "playout.h"
#include "ladder.h"
double calcu_feature_value(Board *b,int tz,int color){
	color = flip_color(b->lm1c);
	
	float f1[] = {0.17,24.37};
	float f2[] = {30.68,0.53,2.88,3.43,0.30};
	float f3[] = {11.37,0.70};
	float f4[] = {0.06};
	float f5[] = {1.58,10.24,1.70};
	float f6[] = {0.89,1.49,1.75,1.28};
	float f7[] = {4.32,2.84,2.22,1.58,1.47,1.36,1.25,1.14,1.03,0.92,0.81,0.70,0.59,0.46,0.33,0.21};
	float f8[] = {3.08,2.38,2.27,1.68,1.56,1.47,1.38,1.29,1.20,1.11,1.02,0.93,0.84,0.75,0.66,0.70};
	float f9[] = {0.04,1.02,2.41,1.41,0.72,0.65,0.68,0.13};


	if ( tz == 0 ) { b->ko_z = 0; return 0; }  // パスの場合



	if ( tz == b->ko_z) return 0; // コウ
	if ( b->data[tz] != 0) return 0; // 既に石がある

	std::vector<int> oppgroups;//id
	//std::vector<int> frigroups;//id
	std::vector<int> lib1_opps;//id
	std::vector<int> lib1_oppz;
	std::vector<int> spaces;//4方向の空白のz
	std::vector<int> walls;//4方向の盤外のz

	int un_col = flip_color(color); // 相手の石の色

	int take_sum = 0;	//// 取れる石
	int ko_maybe = 0;    // コウになるかもしれない場所
	int near_last_move = 0;
	int i;
	std::vector<int> fri_unsafe;
	std::vector<int> fri_safe;

	for (i=0;i<4;i++){
		int z = tz+b->dir4[i];
		int c = b->data[z]; // 石の色
		if ( c == 0 ) {
			spaces.push_back(z);
			continue;
		}
		if ( c == color ) {
			int gid = get_rgroup_id(b,b->z_to_groupid[z]);
			//frigroups.push_back(gid);
			if(b->group[gid].lib_z.size()>1)
				fri_safe.push_back(gid);
			else fri_unsafe.push_back(gid);
			continue;
		}
		if ( c == un_col ) {
			int gid = get_rgroup_id(b,b->z_to_groupid[z]);
			oppgroups.push_back(gid);

			if(b->group[gid].lib_z.size()==1){
				lib1_opps.push_back(gid);
				lib1_oppz.push_back(z);
				ko_maybe = z;
				
			}
			if (b->last_move1==z)
				near_last_move = 1;
			
			continue;
		}
		if ( c == 3 ) {
			walls.push_back(z);
			continue;
		}
	}

	if ( fri_safe.size() == 0 && spaces.size() == 0 && lib1_opps.size() == 0 ) return 0; // 自殺手
	if ( walls.size() + fri_safe.size() == 4 ) return 0; // 眼(ルール違反ではない)

	if (fri_unsafe.size()+fri_safe.size()>0)
		ko_maybe = 0;
	
	double gamma = 1.0;
	//float subfv = 1.0;

	//f1,last move is PASS
	if (b->last_move1 != 0)
		gamma*= f1[0];
	else
		gamma*= f1[1];

	//f2,capture group
	if(lib1_opps.size()>0){}
	{
		//gamma*= 4.5;
		//f2c0,String contiguous to new string in atari
		std::vector<int> atari_fri_group;
		vector<int>::iterator iter ;
		for (int i = 0; i < lib1_opps.size(); ++i)
		{
			std::vector<int> *frigids;
			frigids = &b->group[lib1_opps[i]].neighbor_opp;

			for (iter = frigids->begin(); iter != frigids->end(); iter++)
			 {
			 	if (b->group[*iter].lib_z.size()==1)
			 	{
			 		atari_fri_group.push_back(*iter);
			 	}
			 } 
		}
		if (atari_fri_group.size()>0)
			gamma*= f2[0];

		
		//f2c1,Re-capture previous move
		int f2c1_flag = 0;
		for (int i = 0; i < lib1_oppz.size(); ++i)
		{
			if (lib1_oppz[i]==b->last_move1)
			{
				gamma*= f2[1];
				f2c1_flag = 1;
			}
		}
		//f2c2,Prevent connection to previous move
		//b->z_to_groupid[b->last_move1];
		if (f2c1_flag==0&&near_last_move)
			gamma *=f2[2];

		Board ladderb = *b;
		move(&ladderb,tz,un_col);
		fprintf(stderr, "%d %d\n",get_x(b,tz),get_y(b,tz) );
		int ladder_f = check_ladder_dataunsafe(&ladderb,ladderb.z_to_groupid[tz],3);
		fprintf(stderr, "%d%d\n",tz );
		if(ladder_f!=-1){//String not die in a ladder
			gamma *= f2[3];
		}else{//String in a ladder
			gamma *= f2[4];
		}
		//return gamma;
	}
	//f2 endl,float f2[] = {30.68,0.53,2.88,3.43,0.30};


	//f4 first,slef atari
	std::vector<int> libs_aftermove;
	int f4_flag = 1;
	unique_int_vec(&fri_safe);
	if (spaces.size()<2&&lib1_oppz.size()<2&&ko_maybe==0)
	{
		if (spaces.size()&&lib1_oppz.size())
		{
			f4_flag = 0;
		}else{
			if (spaces.size()!=0)
				libs_aftermove.push_back(spaces[0]);
			if (lib1_oppz.size()!=0)
				libs_aftermove.push_back(lib1_oppz[0]);

			for (int i = 0; i < fri_safe.size(); ++i)
			{
				if (b->group[fri_safe[i]].lib_z.size()>2)
				{
					f4_flag = 0;
					break;
				}else{
					for (int j = 0; j < b->group[fri_safe[i]].lib_z.size(); ++j)
						libs_aftermove.push_back(b->group[fri_safe[i]].lib_z[j]);
				}

			}

			unique_int_vec(&libs_aftermove);
			del_item_int_vec(&libs_aftermove,tz);
			if (libs_aftermove.size()>1) f4_flag = 0;
		}

		if (f4_flag)
			gamma *= f4[0];
	}

 	//f3,extension, {11.37,0.70};
	std::vector<int> lm1_lib1_opps;
	for (i=0;i<4;i++){
		int lm1_near_z = b->last_move1+b->dir4[i];
		//int c = b->data[lm1_near_z]; // 石の色
		if ( b->data[lm1_near_z] == color ) {
			int gid = get_rgroup_id(b,b->z_to_groupid[lm1_near_z]);
			if(b->group[gid].lib_z.size()==1)
				lm1_lib1_opps.push_back(gid);
		}
	}
	unique_int_vec(&lm1_lib1_opps);
	for (int i = 0; i < fri_unsafe.size(); ++i)
	for (int j = 0; j < lm1_lib1_opps.size(); ++j)
	{
		if (fri_unsafe[i] == lm1_lib1_opps[j]){
			Board ladderb = *b;
			move(&ladderb,tz,color);
			int ladder_f = check_ladder_dataunsafe(&ladderb,ladderb.z_to_groupid[tz],3);
		
			if(ladder_f!=-1) gamma *= f3[0];//if not ladder
			else gamma *= f3[1];
		}
	}


	//f5,Atari 
    //1 1.58 Ladder atari
    //2 10.24 Atari4 when there is a ko
	//3 1.70 Other atari
	unique_int_vec(&oppgroups);
	for (int i = 0; i < oppgroups.size(); ++i)
		if (b->group[oppgroups[i]].lib_z.size()==2)
		{
			if (b->ko_z!=0)
				gamma *= f5[1];
			else gamma *= f5[2];
			break;
		}



	//f6-f8, about distance
	int tzx,tzy,delta1x,delta1y,delta2x,delta2y,dis;
	tzx = get_x(b,tz);
	tzy = get_y(b,tz);

	delta1x=abs(tzx-get_x(b,b->last_move1));
	delta1y=abs(tzy-get_y(b,b->last_move1));
	delta2x=abs(tzx-get_x(b,b->last_move2));
	delta2y=abs(tzy-get_y(b,b->last_move2));

	//f6, Distance to border //float f6[] = {0.89,1.49,1.75,1.28};
	if(tzx<4) gamma*= f6[tzx];
	else if(tzx>14) gamma*= f6[18 - tzx] ;
	if(tzy<4)gamma*= f6[tzy];
	else if (tzy>14)gamma*= f6[18 - tzy] ;

	//f7,distance to last move1
	dis = max(delta1x,delta1y) + delta1x + delta1y - 2;
	if (dis>14)
		gamma*=f7[15];
	else
		gamma*=f7[dis];

	//f8,distance to last move2
	dis = max(delta2x,delta2y) + delta2x + delta2y - 2;
	if (dis>14)
		gamma*=f8[15];
	else
		gamma*=f8[dis];
	//f6-f8, endl

	//MC owner,f9[] = {0.04,1.02,2.41,1.41,0.72,0.65,0.68,0.13};
	int step = 8;
	int lv=1;
	int mcn = mcnum_b[tz];

	if (color == 2)
		mcn = step*8-1 - mcn;

	float subf = 1.0;

	if (mcn<step*(lv++))
			subf *= f9[0]; 
	else if (mcn<step*(lv++))
		subf *= f9[1]; 
	else if (mcn<step*(lv++))
		subf *= f9[2]; 
	else if (mcn<step*(lv++))
		subf *= f9[3]; 
	else if (mcn<step*(lv++))
		subf *= f9[4]; 
	else if (mcn<step*(lv++))
		subf *= f9[5]; 
	else if (mcn<step*(lv++))
		subf *= f9[6]; 
	else if (mcn<step*(lv++))
		subf *= f9[7]; 

	//return subf;
	gamma*=subf;

	
	return gamma;
}
/*
Pass 
1 0.17 Previous move is not a pass
2 24.37 Previous move is a pass

Capture 
1 30.68 String contiguous to new string in atari
2 0.53 Re-capture previous move
3 2.88 Prevent connection to previous move
4 3.43 String not in a ladder
5 0.30 String in a ladder

Extension 
1 11.37 New atari, not in a ladder
2 0.70 New atari, in a ladder

Self-atari 
1 0.06

Atari 
1 1.58 Ladder atari
2 10.24 Atari when there is a ko
3 1.70 Other atari


Distance to border 
1 0.89
2 1.49
3 1.75
4 1.28
Distance to 2 4.32 d(x, y) = |x| + |y| + max(|x|, |y|)
previous move 3 2.84
4 2.22
5 1.58
. . . . . .
16 0.33
17 0.21
Distance to 2 3.08
the move before 3 2.38
the previous move 4 2.27
5 1.68
. . . . . .
16 0.66
17 0.70
MC Owner 
1 0.04 0 − 7
2 1.02
*/