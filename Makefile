patterngatherer:patterngatherer.o board.o common.o feature.o playout.o ladder.o pattern.o
	g++ -g -Wall -o $@ $^ 
patterngatherer.o: board.h common.h feature.h playout.h ladder.o pattern.o
.cpp.o:
	g++ -g -Wall  -c $< 

clean: 
	rm -f patterngatherer *.o
